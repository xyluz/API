const models = require('./../models/index');
const token = require('./../controllers/tokens');
const mailer = require('./mailer');
const Op = require('Sequelize').Op;
 

friendrequests = {};

friendrequests.options = (data,callback)=>{

	callback(200,data.headers);
	
}

/**
 * @api {post} /friendrequests Send New Friend Request 
 *
 * @apiName sendFriendRequest
 * @apiGroup Friends
 * @apiHeader {String} uuid Authorization UUID .
 * @apiHeader {String} Token Authorization Token.
 * @apiDescription The endpoint send friend request
 * @apiParam {String} request_sender uuid of the user sending the friend request  
 * @apiParam {String} request_receiver uuid of the user to receive the friend request
 *
 *@apiSuccessExample Success-Response:
 *HTTP/1.1 200 OK
{
    "Success": "Friend Request Sent"
}
 *@apiErrorExample Error-Response:
 *HTTP/1.1 405 Bad Request
{
    "Error": "Request already exists, close that request first before sending another"
}

 */

friendrequests.post = (data,callback)=>{

    let uuidHeader = typeof(data.headers.uuid) == 'string' && data.headers.uuid.trim().length > 0 ? data.headers.uuid.trim() : false;
    let tokenHeader = typeof(data.headers.token) == 'string' && data.headers.token.trim().length > 0 ? data.headers.token.trim() : false;
    
    let requestReceiver = typeof(data.payload.requestReceiver) == 'string' && data.payload.requestReceiver.trim().length > 0 ? data.payload.requestReceiver.trim() : false;

	if( 
		tokenHeader && 
        uuidHeader &&
        requestReceiver
		){

            if(uuidHeader == requestReceiver){
                callback(401,{'Error':'You cannot send request to yourself'});
            }

            token.verify(uuidHeader,tokenHeader).then((result)=>{
			
                if(!result){
                    callback(400,{'Error':'Token Mismatch or expired'});
                }	
    
            })
            .then(()=>{	

                let dataObject = {					
					requestSender:uuidHeader,
					requestReceiver: requestReceiver
				}
               
				models.FriendRequest 
					.findOrCreate({where:{                        
                        [Op.or]: [
                            {requestSender:uuidHeader, requestReceiver:requestReceiver},
                            {requestSender:requestReceiver, requestReceiver:uuidHeader}
                        ]                            
                    }, defaults: dataObject })
					.then(([gotten,created]) => {
                       
						if(created){
                           
							if(gotten){
								callback(200,{'Success':'Friend Request Sent'});
							}else{
								callback(500,{'Error':'Friend Request was not sent, contact admin'});
							}

						}else{
							callback(422,{'Error':'Friend Request already exits'});
						}						
					}).catch((err)=>{
						callback(500,{'Error':err});
						// callback(500,{'Error':"something went wrong, post not created"});
					});

            })
            .catch((err)=>{
                callback(500,{'Error':err});
                // callback(500,{'Error':"something went wrong, post not created"});
            });           

        }else{
           
                let errorObject = [];
        
                if(!token){
                    errorObject.push('Token you supplied is not valid or has expired');
                }
                if(!uuid){
                    errorObject.push('UUID you supplied is invalid or expired');
                }
                if(!requestSender){
                    errorObject.push('Required Parameter request_sender is missing or invalid');
                }
                if(!requestReceiver){
                    errorObject.push('Required Parameter request_receiver is missing or invalid');
                }
        
                callback(400,{'Error':errorObject});       
            
        }
}

/**
 * @api {get} /friendrequests/:uuid Get Pending Friend Request 
 *
 * @apiName getPendingFriendRequest
 * @apiGroup Friends
 * @apiHeader {String} uuid Authorization UUID .
 * @apiHeader {String} Token Authorization Token.
 * @apiDescription The endpoint returns all pending request sent by this user
 * @apiParam {String} uuid uuid of the user  
 *
 *@apiSuccessExample Success-Response:
 *HTTP/1.1 200 OK
{
    "for": [
        {   
            "uuid": "34dee2a7-7b19-49a1-b853-e4b6eaa84969",
            "user": [
                {
                    "id": 1,
                    "uuid": "84b98718-04df-4d4b-a6ac-e8b9981fb5ba",
                    "email": "franko4don@gmail.com",
                    "password": "70d57cc1f61eeec2306a9775a369a1641bd8bee62751554f0e638c06974eb1d6",
                    "phone": "07037219055",
                    "dob": "04/05/2018",
                    "tosAgreement": 1,
                    "provider": "email",
                    "created_at": "2019-02-20T21:37:14.000Z",
                    "updated_at": "2019-02-20T21:37:14.000Z",
                    "status": 1
                }
            ],
            "profile": [
                {
                    "id": 2,
                    "uuid": "84b98718-04df-4d4b-a6ac-e8b9981fb5ba",
                    "nationality_origin": "Nigeria",
                    "nationality_residence": "Nigeria",
                    "state": "Delta",
                    "lga": "Lga of residence",
                    "firstName": "franklin",
                    "lastName": "Nwanze",
                    "photo": "https://res.cloudinary.com/tribenigeria-com/image/upload/v1550714823/zjb0nopimvhefa2nbuc6.jpg",
                    "created_at": "2019-02-21T01:59:30.000Z",
                    "updated_at": "2019-03-22T08:57:10.000Z",
                    "background": "https://res.cloudinary.com/tribenigeria-com/image/upload/v1550714782/epa7arhqgv8fpapb7wje.jpg"
                }
            ]
        },
        {
            "uuid": "34dee2a7-7b19-49a1-b853-e4b6eaa84969",
            "user": [
                {
                    "id": 1,
                    "uuid": "84b98718-04df-4d4b-a6ac-e8b9981fb5ba",
                    "email": "franko4don@gmail.com",
                    "password": "70d57cc1f61eeec2306a9775a369a1641bd8bee62751554f0e638c06974eb1d6",
                    "phone": "07037219055",
                    "dob": "04/05/2018",
                    "tosAgreement": 1,
                    "provider": "email",
                    "created_at": "2019-02-20T21:37:14.000Z",
                    "updated_at": "2019-02-20T21:37:14.000Z",
                    "status": 1
                }
            ],
            "profile": [
                {
                    "id": 2,
                    "uuid": "84b98718-04df-4d4b-a6ac-e8b9981fb5ba",
                    "nationality_origin": "Nigeria",
                    "nationality_residence": "Nigeria",
                    "state": "Delta",
                    "lga": "Lga of residence",
                    "firstName": "franklin",
                    "lastName": "Nwanze",
                    "photo": "https://res.cloudinary.com/tribenigeria-com/image/upload/v1550714823/zjb0nopimvhefa2nbuc6.jpg",
                    "created_at": "2019-02-21T01:59:30.000Z",
                    "updated_at": "2019-03-22T08:57:10.000Z",
                    "background": "https://res.cloudinary.com/tribenigeria-com/image/upload/v1550714782/epa7arhqgv8fpapb7wje.jpg"
                }
            ]
        }
    ],
    "from": [
        {
            "uuid": "34dee2a7-7b19-49a1-b853-e4b6eaa84969",
            "user": [
                {
                    "id": 1,
                    "uuid": "84b98718-04df-4d4b-a6ac-e8b9981fb5ba",
                    "email": "franko4don@gmail.com",
                    "password": "70d57cc1f61eeec2306a9775a369a1641bd8bee62751554f0e638c06974eb1d6",
                    "phone": "07037219055",
                    "dob": "04/05/2018",
                    "tosAgreement": 1,
                    "provider": "email",
                    "created_at": "2019-02-20T21:37:14.000Z",
                    "updated_at": "2019-02-20T21:37:14.000Z",
                    "status": 1
                }
            ],
            "profile": [
                {
                    "id": 2,
                    "uuid": "84b98718-04df-4d4b-a6ac-e8b9981fb5ba",
                    "nationality_origin": "Nigeria",
                    "nationality_residence": "Nigeria",
                    "state": "Delta",
                    "lga": "Lga of residence",
                    "firstName": "franklin",
                    "lastName": "Nwanze",
                    "photo": "https://res.cloudinary.com/tribenigeria-com/image/upload/v1550714823/zjb0nopimvhefa2nbuc6.jpg",
                    "created_at": "2019-02-21T01:59:30.000Z",
                    "updated_at": "2019-03-22T08:57:10.000Z",
                    "background": "https://res.cloudinary.com/tribenigeria-com/image/upload/v1550714782/epa7arhqgv8fpapb7wje.jpg"
                }
            ]
        }
    ]
}
 *@apiSuccessExample Success-Response:
 *HTTP/1.1 200 OK
 {
    "for": [
        {
            "user": [
                {
                    "id": 5,
                    "uuid": "9b494e70-3f93-4181-bcd3-87f0ce1332ec",
                    "email": "everistusolumese@gmail.com",
                    "password": "2231306d33a58824b362898c6a1a0eb5907c74cd76928960df85d501eba90fcb",
                    "phone": "09031866339",
                    "dob": "1980-01-31T23:00:00.000Z",
                    "tosAgreement": 1,
                    "provider": "email",
                    "created_at": "2019-03-10T08:52:28.000Z",
                    "updated_at": "2019-03-10T08:52:28.000Z",
                    "status": 1
                }
            ],
            "profile": [
                {
                    "id": 3,
                    "uuid": "9b494e70-3f93-4181-bcd3-87f0ce1332ec",
                    "nationality_origin": "Vanuatu",
                    "nationality_residence": "Nigeria",
                    "state": "N/A",
                    "lga": "N/A",
                    "firstName": "Everistus",
                    "lastName": "Olumese",
                    "photo": "https://res.cloudinary.com/xyluz/image/upload/v1553172303/WEB/chelsea_ksbydb.png",
                    "created_at": "2019-03-21T12:45:04.000Z",
                    "updated_at": "2019-03-21T12:45:04.000Z",
                    "background": "false"
                }
            ]
        }
    ],
    "from": []
}
 *@apiSuccessExample Success-Response:
 *HTTP/1.1 200 OK
 {
     []
 }
 *@apiErrorExample Error-Response:
 *HTTP/1.1 404 Bad Request
{
    "Error": [
        "You need to provide user uuid as a parameter"
    ]
}
 */

friendrequests.get = (data,callback)=>{
    
    let uuidHeader = typeof(data.headers.uuid) == 'string' && data.headers.uuid.trim().length > 0 ? data.headers.uuid.trim() : false;
    let tokenHeader = typeof(data.headers.token) == 'string' && data.headers.token.trim().length > 0 ? data.headers.token.trim() : false;

    let param = typeof(data.param) == 'string' && data.param.trim().length > 0 ? data.param.trim() : false;

    // let queryObject = Object.keys(data.queryStringObject).length > 0 && typeof(data.queryStringObject) == 'object' ? data.queryStringObject : false;

    let page = typeof(data.queryStringObject.page) == 'string'  ? data.queryStringObject.page : 1; 
	let limit = typeof(data.queryStringObject.limit) == 'string' ? data.queryStringObject.limit : 10;
	let sort = typeof(data.queryStringObject.sort) == 'string' && data.queryStringObject.sort.trim().length > 0 && (data.queryStringObject.sort.trim() == 'ASC' || 'DESC') ? data.queryStringObject.sort.trim() : 'DESC';

    if( 
		tokenHeader && 
		uuidHeader &&
        param
		){
            token.verify(uuidHeader,tokenHeader).then((result)=>{
			
				if(!result){
					callback(400,{'Error':'Token Mismatch or expired'});
				}	
	
			})
			.then(()=>{
            
                models.FriendRequest.findAndCountAll({
                    where:{
                        [Op.or]: [
                            {requestSender:uuidHeader},
                            {requestReceiver:uuidHeader}
                        ]   
                    },
					offset: page, 
					limit: limit,
					order: [['createdAt', sort]]
					
				})
				.then((friendRequests)=>{						
					callback(200,{friendRequests});
				})
				.catch(err=>callback(500,{'Error':err}));

            })
            .catch(err=>callback(400,{'Error':err}));

        }else{

		let errorObject = [];

		if(!tokenHeader){
			errorObject.push('Token you supplied is not valid or has expired');
		}
		if(!uuidHeader){
			errorObject.push('UUID you supplied is invalid or expired');
        }
        
        if(!param){
            errorObject.push('You need to provide user uuid as a parameter');
        }

		callback(400,{'Error':errorObject});

	}
}


friendrequests.put = (data,callback)=>{

    callback(200,{'Success':'Friend Requests put endpoint'});

}

/**
 * @api {delete} /friendrequests/:uuid Delete Friend Request 
 * @apiName deleteFriendRequest
 * @apiGroup Friends
 * @apiHeader {String} uuid Authorization UUID.
 * @apiHeader {String} Token Authorization Token.
 * @apiDescription The endpoint rejects / deletes a friend request
 * @apiParam {String} uuid uuid of the request to be deleted 
 *@apiSuccessExample Success-Response:
 *HTTP/1.1 200 OK
 *{
 *   "Success": "Friend Request Deleted"
 *}
 *@apiErrorExample Error-Response:
 *HTTP/1.1 400 Bad Request
 *{
 *   "Error": [
 *       "Friend Request uuid not valid"
 *   ]
 *}
 * @apiErrorExample Error-Response:
 *HTTP/1.1 404 Bad Request
 *{
 *   "Error": [
 *       "Friend Request not found"
 *   ]
 *}
 */


friendrequests.delete = (data,callback)=>{

    let param = typeof(data.param) == 'string' && data.param.trim().length > 0 ? data.param.trim() : false;
	let tokenHeader = typeof(data.headers.token) == 'string' && data.headers.token.trim().length > 0 ? data.headers.token.trim() : false;
	let uuidHeader = typeof(data.headers.uuid) == 'string' && data.headers.uuid.trim() ? data.headers.uuid.trim() : false;

	if( 
		tokenHeader && 
		uuidHeader &&
		param 
		){

			token.verify(uuidHeader,tokenHeader).then((result)=>{
					
				if(!result){
						callback(400,{'Error':'Token Mismatch or expired'});
				}
	
			})
			.then(()=>{
				models.FriendRequest.findOne({where: {uuid:param}})
				.then((fr)=>{
					if(fr){
						let destroy = fr.destroy();
						if(destroy){
							callback(404,{'Success':'Friend Request Deleted'});
						}else{
							callback(500,{'Error':'Friend Request not deleted, something went wrong'});
						}
					}else{
						callback(404,{'Error':'Friend Request not found'});	
					}
				})
				.catch(err=>callback(500,err));
			})
			.catch(err=>callback(500,err));


	}else{

		let errorObject = [];

		if(!token){
			errorObject.push('Token you supplied is not valid or expired');
		}
		if(!uuidHeader){
			errorObject.push('uuid in the header not found');
		}
		if(!post){
			errorObject.push('Post uuid not valid');
		}

		callback(400,{'Error':errorObject});
	}

}



module.exports = friendrequests;