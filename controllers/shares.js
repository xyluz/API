const models = require('./../models/index');
const token = require('./../controllers/tokens');


shares = {};
 
shares.options = (data,callback)=>{

	callback(200,data.headers);
	
}

shares.post = (data,callback)=>{

	let uuidHeader = typeof(data.headers.uuid) == 'string' && data.headers.uuid.trim().length > 0 ? data.headers.uuid.trim() : false;
	let tokenHeader = typeof(data.headers.token) == 'string' && data.headers.token.trim().length > 0 ? data.headers.token.trim() : false;
	let post = typeof(data.payload.post) == 'string' && data.payload.post.trim().length > 0 ? data.payload.post.trim() : false;

	if(uuidHeader && tokenHeader && post){

		token.verify(uuidHeader,tokenHeader).then((result)=>{
			
			if(!result){
				callback(400,{'Error':'Token Mismatch or expired'});
			}	

		})
		.then(()=>{
			
			let dataObject = {				
				userId:uuidHeader,
				postId: post
			}

			models.Share 
				.create( dataObject )
				.then((share) => {
					if(share){
						callback(200,{'Success':'Share Registered'});
					}else{
						callback(500,{'Error':'Share was not registered, contact admin'});
					}						
				}).catch((err)=>{
					callback(500,{'Error':err});
					// callback(500,{'Error':"something went wrong, post not created"});
				});

		})
		.catch(err=>callback(400,{'Error':err}));
		
	}else{
		let errorObject = [];
		if(!token){
			errorObject.push('Token is invalid');
		}

		if(!user){
			errorObject.push('User UUID invalid');
		}
		if(!post){
			errorObject.push('Post uuid invalid');		
		}
		callback(400,{'Error':errorObject});
	}

}

shares.get = (data,callback)=>{

	let uuidHeader = typeof(data.headers.uuid) == 'string' && data.headers.uuid.trim().length > 0 ? data.headers.uuid.trim() : false;
	let tokenHeader = typeof(data.headers.token) == 'string' && data.headers.token.trim().length > 0 ? data.headers.token.trim() : false;
	let post = typeof(data.param) == 'string' && data.param.trim().length > 0 ? data.param.trim() : false;

	let queryObject = Object.keys(data.queryStringObject).length > 0 && typeof(data.queryStringObject) == 'object' ? data.queryStringObject : false;

	let page = typeof(queryObject.page) == 'string'  ? queryObject.page : 1; 
	let limit = typeof(queryObject.limit) == 'string' ? queryObject.limit : 10;
	let sort = typeof(queryObject.sort) == 'string' && queryObject.sort.trim().length > 0 && (queryObject.sort.trim() == 'ASC' || 'DESC') ? queryObject.sort.trim() : 'DESC';

	if(uuidHeader && tokenHeader && post){

		token.verify(uuidHeader,tokenHeader).then((result)=>{
			
			if(!result){
				callback(400,{'Error':'Token Mismatch or expired'});
			}	

		})
		.then(()=>{

			models.Share.findAndCountAll({
				where:{postId:post}, 
				offset: page, 
				limit: limit,
				order: [['createdAt', sort]], 
				include: [
					{model:models.User,
						include:[{model:models.Profile}]
					}] 
				})
				.then((shares)=>{						
					callback(200,{shares});
				})
				.catch(err=>callback(500,{'Error':err}));

		})
		.catch(err=>callback(400,{'Error':err}));

	}else{
		let errorObject = [];
		if(!token){
			errorObject.push('Token is invalid');
		}

		if(!user){
			errorObject.push('User UUID invalid');
		}
		if(!post){
			errorObject.push('Post uuid invalid');		
		}
		callback(400,{'Error':errorObject});
	}
}


module.exports = shares;
