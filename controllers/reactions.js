const models = require('./../models/index');
const token = require('./../controllers/tokens');

reactions = {};

reactions.options = (data,callback)=>{

	callback(200,data.headers);
	
}

reactions.post = (data,callback)=>{

	let post = typeof(data.payload.post) == 'string' && data.payload.post.trim().length > 0 ? data.payload.post.trim() : false;
	let uuidHeader = typeof(data.headers.uuid) == 'string' && data.headers.uuid.trim().length > 0 ? data.headers.uuid.trim() : false;
	let tokenHeader = typeof(data.headers.token) == 'string' && data.headers.token.trim().length > 0 ? data.headers.token.trim() : false;


	if( tokenHeader && uuidHeader && post ){

		token.verify(uuidHeader,tokenHeader).then((result)=>{
			
			if(!result){
				callback(400,{'Error':'Token Mismatch or expired'});
			}	

		})
		.then(()=>{			

				let dataObject = {					
					userId:uuidHeader,
					postId: post
				}

				models.Reaction 
					.findOrCreate({where:{userId:uuidHeader,postId:post}, default:dataObject })
					.then(([comment,created]) => {
						if(created){

							if(comment){
								callback(200,{'Success':'Reaction Registered'});
							}else{
								callback(500,{'Error':'Reaction was not registered, contact admin'});
							}

						}else{
							callback(422,{'Error':'User already reacted to this post'})
						}						
					}).catch((err)=>{
						callback(500,{'Error':err});
						// callback(500,{'Error':"something went wrong, post not created"});
					});

		})
		.catch((err)=>{
			callback(500,{'Error':err});
			// callback(500,{'Error':"something went wrong, post not created"});
		});

		
	}else{
		let errorObject = [];

		if(!post){
			errorObject.push('Post uuid is required');
		}
		if(!uuidHeader){
			errorObject.push('uuid is required');
		}
		if(!tokenHeader){
			errorObject.push('token is required');
		}
		callback(400,{'Error':errorObject});
	}	

}


reactions.get = (data,callback)=>{

	let param = typeof(data.param) == 'string' && data.param.trim().length > 0 ? data.param.trim() : false;
	let tokenHeader = typeof(data.headers.token) == 'string' && data.headers.token.trim().length > 0 ? data.headers.token.trim() : false;
	let uuidHeader = typeof(data.headers.uuid) == 'string' && data.headers.uuid.trim() ? data.headers.uuid.trim() : false;

	let queryObject = Object.keys(data.queryStringObject).length > 0 && typeof(data.queryStringObject) == 'object' ? data.queryStringObject : false;

	let page = typeof(queryObject.page) == 'string'  ? queryObject.page : 1; 
	let limit = typeof(queryObject.limit) == 'string' ? queryObject.limit : 10;
	let sort = typeof(queryObject.sort) == 'string' && queryObject.sort.trim().length > 0 && (queryObject.sort.trim() == 'ASC' || 'DESC') ? queryObject.sort.trim() : 'DESC';

	if( tokenHeader && 
		uuidHeader &&
		param 
		){

			token.verify(uuidHeader,tokenHeader).then((result)=>{
			
				if(!result){
					callback(400,{'Error':'Token Mismatch or expired'});
				}	
	
			})
			.then(()=>{
				
				models.Reaction.findAndCountAll({
					offset: page, 
					limit: limit,
					order: [['createdAt', sort]], 
					include: [{model:models.User,
						include:[{model:models.Profile}]
					}] 
				})
				.then((reactions)=>{						
					callback(200,{reactions});
				})
				.catch(err=>callback(500,{'Error':err}));

			})
			.catch(err=>callback(400,{'Error':err}));
		
	}else{
		let errorObject = [];
		if(!param){
			errorObject.push('Missing required parameter,post uuid');
		}
		if(!uuidHeader){
			errorObject.push('Missing required parameter, header uuid');
		}
		if(!tokenHeader){
			errorObject.push('Missing required parameter,header token');
		}
		callback(400,{'Error':errorObject});
	}

}

reactions.delete = (data,callback)=>{

	//get a user profile
	let param = typeof(data.param) == 'string' && data.param.trim().length > 0 ? data.param.trim() : false;
	let tokenHeader = typeof(data.headers.token) == 'string' && data.headers.token.trim().length > 0 ? data.headers.token.trim() : false;
	let uuidHeader = typeof(data.headers.uuid) == 'string' && data.headers.uuid.trim() ? data.headers.uuid.trim() : false;

	if( 
		tokenHeader && 
		uuidHeader &&
		param 
		){

			token.verify(uuidHeader,tokenHeader).then((result)=>{
					
				if(!result){
					callback(400,{'Error':'Token Mismatch or expired'});
				}
	
			})
			.then(()=>{
				models.Reaction.findOne({where: {id:param}})
				.then((reaction)=>{
					if(reaction){
						let destroy = reaction.destroy();
						if(destroy){
							callback(404,{'Success':'Reaction Removed'});
						}else{
							callback(500,{'Error':'Reaction not deleted, something went wrong'});
						}
					}else{
						callback(404,{'Error':'Reaction not found'});	
					}
				})
				.catch(err=>callback(500,err));
			})
			.catch(err=>callback(500,err));


	}else{

		let errorObject = [];

		if(!token){
			errorObject.push('Token you supplied is not valid or expired');
		}
		if(!uuidHeader){
			errorObject.push('uuid in the header not found');
		}
		if(!post){
			errorObject.push('Comment uuid not valid');
		}

		callback(400,{'Error':errorObject});
	}


}


module.exports = reactions;
