const models = require('./../models/index');
const token = require('./../controllers/tokens');

let posts = {};


posts.options = (data,callback)=>{

	callback(200,data.headers);
	
} 

posts.post = (data,callback)=>{ 

	let post = typeof(data.payload.post) == 'string' && data.payload.post.trim().length > 0 ? data.payload.post.trim() : false;
	let location = typeof(data.payload.location) == 'string' && data.payload.location.trim().length > 0 ? data.payload.location.trim() : 'unspecified';
	let attachment = typeof(data.payload.attachment) == 'string' && data.payload.attachment.trim().length > 0  ? data.payload.attachment : null;
	let post_type = typeof(data.payload.post_type) == 'string' && data.payload.post_type.trim().length > 0  ? data.payload.post_type : 'post';
	let uuidHeader = typeof(data.headers.uuid) == 'string' && data.headers.uuid.trim().length > 0 ? data.headers.uuid.trim() : false;
	let tokenHeader = typeof(data.headers.token) == 'string' && data.headers.token.trim().length > 0 ? data.headers.token.trim() : false;

	if(tokenHeader && uuidHeader){

		token.verify(uuidHeader,tokenHeader).then((result)=>{
			
			if(!result){
					callback(400,{'Error':'Token Mismatch or expired'});
			}	

		})
		.then(()=>{

			if(post){

				let data = {
					post:post,
					location: location,
					attachment:attachment,
					post_type: post_type,
					userId:uuidHeader					
				}
				 models.Post 
					.create( data )
					.then((post) => {
						if(post){
							callback(200,{'Success':'Post Created'});
						}else{
							callback(500,{'Error':'Post was not created, contact admin'});
						}						
					}).catch((err)=>{
						callback(500,{'Error':err});
						// callback(500,{'Error':"something went wrong, post not created"});
					});
			}else{
				callback(400,{'Error':'Request is missing required field: post'});
			}				

		}).catch((err)=>{
				//TODO: This should be optimzed
				console.log(err);
				callback(500,err);
		});	

	}else{
		let errorObject = [];

		if(!tokenHeader){
			errorObject.push('Missing required paramer, token');
		}
		if(!uuidHeader){
			errorObject.push('Missing required paramer, uuid');
		}
		if(!post){
			errorObject.push('Missing required paramer, post');
		}
	}

}

posts.get = (data,callback)=>{
	
	let tokenHeader = typeof(data.headers.token) == 'string' && data.headers.token.trim().length > 0 ? data.headers.token.trim() : false;
	let uuidHeader = typeof(data.headers.uuid) == 'string' && data.headers.uuid.trim() ? data.headers.uuid.trim() : false;
	let post = typeof(data.param) == 'string' && data.param.trim().length > 0 ? data.param.trim() : false;
	let queryObject = Object.keys(data.queryStringObject).length > 0 && typeof(data.queryStringObject) == 'object' ? data.queryStringObject : false;

	let page = typeof(queryObject.page) == 'string'  ? queryObject.page : 1; 
	let limit = typeof(queryObject.limit) == 'string' ? queryObject.limit : 10;
	let sort = typeof(queryObject.sort) == 'string' && queryObject.sort.trim().length > 0 && (queryObject.sort.trim() == 'ASC' || 'DESC') ? queryObject.sort.trim() : 'DESC';

	if( 
		tokenHeader && 
		uuidHeader 
		 
		){

			if(tokenHeader && uuidHeader){

				token.verify(uuidHeader,tokenHeader).then((result)=>{
					
					if(!result){
							callback(400,{'Error':'Token Mismatch or expired'});
					}	
		
				})
				.then(()=>{

					if(!post && !queryObject.user){

						models.Post.findAndCountAll({offset: page, limit: limit,order: [['createdAt', sort]], include: [{model:models.User,include:[{model:models.Profile}]},{model:models.View},{model:models.Comment},{model:models.Share},{model:models.Reaction}] })
						.then((posts)=>{						
							callback(200,{posts});
						})
						.catch(err=>callback(500,{'Error':err}));

					}

					if(post && !queryObject.user){

						models.Post.findAndCountAll({where: {id:post},offset: page, limit: limit, order: [['createdAt', sort]], include: [{model:models.User, include: [{model:models.Profile}]},{model:models.View},{model:models.Comment},{model:models.Share},{model:models.Reaction}] })
						.then((posts)=>{						
							callback(200,{posts});
						})
						.catch(err=>callback(500,{'Error':err}));

					}

					if(queryObject && queryObject.user){

						models.Post.findAndCountAll({where: {userId:queryObject.user},offset: page, limit: limit, order: [['createdAt', sort]], include: [{model:models.User, include: [{model:models.Profile}]},{model:models.View},{model:models.Comment},{model:models.Share},{model:models.Reaction}] })
						.then((posts)=>{						
							callback(200,{posts});
						})
						.catch(err=>callback(500,{'Error':err}));

					}					

				}).catch((err)=>{
					//TODO: This should be optimzed
					console.log(err);
					callback(500,err);
				});
		
			}else{
					let errorObject = [];
			
					if(!token){
						errorObject.push('Token you supplied is not valid or expired');
					}
					if(!uuidHeader){
						errorObject.push('uuid in the header not found');
					}
			
					callback(400,{'Error':errorObject});
				}
		}
}



posts.put = (data,callback)=>{

	let uuidHeader = typeof(data.headers.uuid) == 'string' && data.headers.uuid.trim().length > 0 ? data.headers.uuid.trim() : false;
	let tokenHeader = typeof(data.headers.token) == 'string' && data.headers.token.trim().length > 0 ? data.headers.token.trim() : false;
	let param = typeof(data.param) == 'string' && data.param.trim().length > 0 ? data.param.trim() : false;

	if(tokenHeader && uuidHeader){

		token.verify(uuidHeader,tokenHeader).then((result)=>{
					
			if(!result){
					callback(400,{'Error':'Token Mismatch or expired'});
			}

		})
		.then(()=>{

			models.Post.findOne({where: {uuid:param}})
			.then((post)=>{
		
				if(post){
				
					let postMade = typeof(data.payload.post) == 'string' && data.payload.post.trim().length > 0 ? data.payload.post.trim() : post.post;
					let post_type =  typeof(data.payload.post_type) == 'string' && data.payload.post_type.trim().length > 0 ? data.payload.post_type.trim() : post.post_type;
					let location = typeof(data.payload.location) == 'string' && data.payload.location.trim().length > 0 ? data.payload.location.trim() : post.location;
					let attachment = typeof(data.payload.attachment) == 'object' && data.payload.attachment.length > 0  ? data.payload.attachment : post.attachment;

					let dataObject = {
						post:postMade,
						location: location,
						attachment:attachment,
						post_type: post_type,
						userId:uuidHeader					
					}
					
					post.update(dataObject)
					.then(() => {
						callback(200,{'Success':'Post Update done'});
					})
					.catch(err=>callback(500,err));

				}else{
					callback(404,{'Error':'Post not found'});
				}

			}).catch(err=>callback(500,err));


		}).catch((err)=>{
			//TODO: This should be optimzed
			console.log(err);
			callback(500,err);
		});

	}else{
		callback(400,{'Error':'Token Invalid or expired'});
	}
	
}

posts.delete = (data,callback)=>{ 
	//get a user profile
	let param = typeof(data.param) == 'string' && data.param.trim().length > 0 ? data.param.trim() : false;
	let tokenHeader = typeof(data.headers.token) == 'string' && data.headers.token.trim().length > 0 ? data.headers.token.trim() : false;
	let uuidHeader = typeof(data.headers.uuid) == 'string' && data.headers.uuid.trim() ? data.headers.uuid.trim() : false;

	if( 
		tokenHeader && 
		uuidHeader &&
		param 
		){

			token.verify(uuidHeader,tokenHeader).then((result)=>{
					
				if(!result){
						callback(400,{'Error':'Token Mismatch or expired'});
				}
	
			})
			.then(()=>{
				models.Post.findOne({where: {uuid:param}})
				.then((post)=>{
					if(post){
						let destroy = post.destroy();
						if(destroy){
							callback(404,{'Success':'Post Deleted'});
						}else{
							callback(500,{'Error':'Post not deleted, something went wrong'});
						}
					}else{
						callback(404,{'Error':'Post not found'});	
					}
				})
				.catch(err=>callback(500,err));
			})
			.catch(err=>callback(500,err));


	}else{

		let errorObject = [];

		if(!token){
			errorObject.push('Token you supplied is not valid or expired');
		}
		if(!uuidHeader){
			errorObject.push('uuid in the header not found');
		}
		if(!param){
			errorObject.push('Post uuid not valid');
		}

		callback(400,{'Error':errorObject});
	}
}


module.exports = posts;
