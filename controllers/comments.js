const mailer = require('./mailer'); //TODO: send email when comment is made
const models = require('./../models/index');
const token = require('./../controllers/tokens');


let comments = {};

comments.options = (data,callback)=>{

	callback(200,data.headers);
	
}

comments.post = (data,callback)=>{

	let comment = typeof(data.payload.comment) == 'string' && data.payload.comment.trim().length > 0 ? data.payload.comment.trim() : false;
	let ref = typeof(data.payload.ref) == 'string' && data.payload.ref.trim().length > 0 ? data.payload.ref.trim() : false;
	let uuidHeader = typeof(data.headers.uuid) == 'string' && data.headers.uuid.trim().length > 0 ? data.headers.uuid.trim() : false;
	let tokenHeader = typeof(data.headers.token) == 'string' && data.headers.token.trim().length > 0 ? data.headers.token.trim() : false;

	if( tokenHeader && uuidHeader && ref ){

		token.verify(uuidHeader,tokenHeader).then((result)=>{
			
			if(!result){
					callback(400,{'Error':'Token Mismatch or expired'});
			}	

		})
		.then(()=>{
			if(comment){

				let dataObject = {
					comment:comment,
					userId:uuidHeader,
					postId: ref
				}

				models.Comment 
					.create( dataObject )
					.then((comment) => {
						if(comment){
							callback(200,{'Success':'Comment Posted'});
						}else{
							callback(500,{'Error':'Comment was not created, contact admin'});
						}						
					}).catch((err)=>{
						callback(500,{'Error':err});
						// callback(500,{'Error':"something went wrong, post not created"});
					});

			}else{
				callback(405,{'Error':'Missing Required Parameter, Comment'});
			}
		});
	

		
	}else{
		let errorObject = [];

		if(!ref){
			errorObject.push('Ref is required');
		}
		if(!uuidHeader){
			errorObject.push('uuid is required');
		}
		if(!tokenHeader){
			errorObject.push('token is required');
		}
		callback(400,{'Error':errorObject});
	}	

}

comments.get = (data,callback)=>{
	
	let param = typeof(data.param) == 'string' && data.param.trim().length > 0 ? data.param.trim() : false;
	let tokenHeader = typeof(data.headers.token) == 'string' && data.headers.token.trim().length > 0 ? data.headers.token.trim() : false;
	let uuidHeader = typeof(data.headers.uuid) == 'string' && data.headers.uuid.trim() ? data.headers.uuid.trim() : false;

	let queryObject = Object.keys(data.queryStringObject).length > 0 && typeof(data.queryStringObject) == 'object' ? data.queryStringObject : false;

	let page = typeof(queryObject.page) == 'string'  ? queryObject.page : 1; 
	let limit = typeof(queryObject.limit) == 'string' ? queryObject.limit : 10;
	let sort = typeof(queryObject.sort) == 'string' && queryObject.sort.trim().length > 0 && (queryObject.sort.trim() == 'ASC' || 'DESC') ? queryObject.sort.trim() : 'DESC';

	if( tokenHeader && 
		uuidHeader &&
		param 
		){

			token.verify(uuidHeader,tokenHeader).then((result)=>{
			
				if(!result){
					callback(400,{'Error':'Token Mismatch or expired'});
				}	
	
			})
			.then(()=>{
				
				models.Comment.findAndCountAll({offset: page, limit: limit,order: [['createdAt', sort]], include: [{model:models.User,include:[{model:models.Profile}]}] })
					.then((comments)=>{						
						callback(200,{comments});
					})
					.catch(err=>callback(500,{'Error':err}));

			})
			.catch(err=>callback(400,{'Error':err}));
		
	}else{
		let errorObject = [];
		if(!param){
			errorObject.push('Missing required parameter,post uuid');
		}
		if(!uuidHeader){
			errorObject.push('Missing required parameter, header uuid');
		}
		if(!tokenHeader){
			errorObject.push('Missing required parameter,header token');
		}
		callback(400,{'Error':errorObject});
	}
}

comments.put = (data,callback)=>{
	callback(200,{'Success':'You have hit the comment put endpoint'});
}

comments.delete = (data,callback)=>{
	//get a user profile
	let param = typeof(data.param) == 'string' && data.param.trim().length > 0 ? data.param.trim() : false;
	let tokenHeader = typeof(data.headers.token) == 'string' && data.headers.token.trim().length > 0 ? data.headers.token.trim() : false;
	let uuidHeader = typeof(data.headers.uuid) == 'string' && data.headers.uuid.trim() ? data.headers.uuid.trim() : false;

	if( 
		tokenHeader && 
		uuidHeader &&
		param 
		){

			token.verify(uuidHeader,tokenHeader).then((result)=>{
					
				if(!result){
					callback(400,{'Error':'Token Mismatch or expired'});
				}
	
			})
			.then(()=>{
				models.Comment.findOne({where: {id:param}})
				.then((comment)=>{
					if(comment){
						let destroy = comment.destroy();
						if(destroy){
							callback(404,{'Success':'Comment Deleted'});
						}else{
							callback(500,{'Error':'Comment not deleted, something went wrong'});
						}
					}else{
						callback(404,{'Error':'Comment not found'});	
					}
				})
				.catch(err=>callback(500,err));
			})
			.catch(err=>callback(500,err));


	}else{

		let errorObject = [];

		if(!token){
			errorObject.push('Token you supplied is not valid or expired');
		}
		if(!uuidHeader){
			errorObject.push('uuid in the header not found');
		}
		if(!post){
			errorObject.push('Comment uuid not valid');
		}

		callback(400,{'Error':errorObject});
	}

}


module.exports = comments;
