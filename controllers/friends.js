const models = require('./../models/index');
const token = require('./../controllers/tokens');
const mailer = require('./mailer');
const Op = require('Sequelize').Op;
 

friends = {};

friends.options = (data,callback)=>{

	callback(200,data.headers);
	
}

/**
 * @api {post} /friends Accept Friend Request 
 *
 * @apiName createFriend
 * @apiGroup Friends
 * @apiHeader {String} uuid Authorization UUID .
 * @apiHeader {String} Token Authorization Token.
 * @apiDescription The endpoint send friend request
 * @apiParam {String} request_uuid the uuid of the request to be accepted  
 *
 *@apiSuccessExample Success-Response:
 *HTTP/1.1 200 OK
{
    "Success": "Friend Request Accepted"
}

 *@apiErrorExample Error-Response:
 *HTTP/1.1 401 Bad Request
{
    "Error": "Bad Request"
}

 */

friends.post = (data,callback)=>{

    let uuidHeader = typeof(data.headers.uuid) == 'string' && data.headers.uuid.trim().length > 0 ? data.headers.uuid.trim() : false;
    let tokenHeader = typeof(data.headers.token) == 'string' && data.headers.token.trim().length > 0 ? data.headers.token.trim() : false;
    
    let request = typeof(data.payload.request) == 'string' && data.payload.request.trim().length > 0 ? data.payload.request.trim() : false;
    
    if( 
		tokenHeader && 
        uuidHeader &&
        request 

		){

            token.verify(uuidHeader,tokenHeader).then((result)=>{
			
                if(!result){
                    callback(400,{'Error':'Token Mismatch or expired'});
                }	
    
            })
            .then(()=>{	

                models.FriendRequest.findOne({where: {id:request}})
                .then((friendRequest)=>{

                    if(!friendRequest){
                        callback(404,{'Error':'Request not found'});
                    }



                    const {requestReceiver, requestSender} = friendRequest;

                    if(requestReceiver !== uuidHeader && requestSender !== uuidHeader){
                        callback(400,{'Error':'You cannot accept this request, it does not belong to you'});
                    }

                    const dataObject = {
                        userA : requestReceiver,
                        userB : requestSender
                    };

                    models.Friend 
                        .findOrCreate({where:{                        
                            [Op.or]: [
                                {userA: requestReceiver, userB: requestSender},
                                {userA: requestSender, userB: requestReceiver}
                            ]                            
                        }, defaults: dataObject })
                        .then(([gotten,created]) => {

                            if(created){
                                friendRequest.destroy();
                                callback(200,{'Success':'Friend Request Accepted'})
                            }
                            callback(400,{'Error':'Users are already friends','data':gotten});

                        }).catch(err=>console.log(err));

                }).catch(err=>console.log(err));                


            })
            .catch((err)=>{
                callback(500,{'Error':err});
                // callback(500,{'Error':"something went wrong, post not created"});
            });  
             

        }else{
           
                let errorObject = [];
        
                if(!tokenHeader){
                    errorObject.push('Token you supplied is not valid or has expired');
                }
                if(!uuidHeader){
                    errorObject.push('UUID you supplied is invalid or expired');
                }
                if(!request){
                    errorObject.push('Required Parameter request is missing or invalid');
                }               
        
                callback(400,{'Error':errorObject});            
        }
   
}

/**
 * @api {get} /friends/:uuid Get Friends  
 *
 * @apiName getSingleUserFriend
 * @apiGroup Friends
 * @apiHeader {String} uuid Authorization UUID .
 * @apiHeader {String} Token Authorization Token.
 * @apiDescription The endpoint returns all associated friends with the user
 * @apiParam {String} uuid  uuid of the usedr  
 *
 *@apiSuccessExample Success-Response:
 *HTTP/1.1 200 OK
[
    {
        "user": [
            {
                "id": 3,
                "uuid": "48e1f9d6-fc31-40db-bfa9-3ad41dbb9cdf",
                "email": "frank4merry@gmail.com",
                "password": "70d57cc1f61eeec2306a9775a369a1641bd8bee62751554f0e638c06974eb1d6",
                "phone": "07037219054",
                "dob": "04/05/2018",
                "tosAgreement": 1,
                "provider": "email",
                "created_at": "2019-02-21T02:14:54.000Z",
                "updated_at": "2019-02-21T02:14:54.000Z",
                "status": 1
            }
        ],
        "profile": []
    },
    {
        "user": [
            {
                "id": 3,
                "uuid": "48e1f9d6-fc31-40db-bfa9-3ad41dbb9cdf",
                "email": "frank4merry@gmail.com",
                "password": "70d57cc1f61eeec2306a9775a369a1641bd8bee62751554f0e638c06974eb1d6",
                "phone": "07037219054",
                "dob": "04/05/2018",
                "tosAgreement": 1,
                "provider": "email",
                "created_at": "2019-02-21T02:14:54.000Z",
                "updated_at": "2019-02-21T02:14:54.000Z",
                "status": 1
            }
        ],
        "profile": []
    }
]

 *@apiErrorExample Error-Response:
 *HTTP/1.1 401 Bad Request
{
    "Error": [
        "You need to provide user uuid as a parameter"
    ]
}
 */

friends.get = (data,callback)=>{

   //get all friends for a person
   let uuidHeader = typeof(data.headers.uuid) == 'string' && data.headers.uuid.trim().length > 0 ? data.headers.uuid.trim() : false;
   let tokenHeader = typeof(data.headers.token) == 'string' && data.headers.token.trim().length > 0 ? data.headers.token.trim() : false;

   let param = typeof(data.param) == 'string' && data.param.trim().length > 0 ? data.param.trim() : false;

   let queryObject = Object.keys(data.queryStringObject).length > 0 && typeof(data.queryStringObject) == 'object' ? data.queryStringObject : false;

   let page = typeof(data.queryStringObject.page) == 'string'  ? data.queryStringObject.page : 0; 
   let limit = typeof(data.queryStringObject.limit) == 'string' ? data.queryStringObject.limit : 10;
   let sort = typeof(data.queryStringObject.sort) == 'string' && data.queryStringObject.sort.trim().length > 0 && (data.queryStringObject.sort.trim() == 'ASC' || 'DESC') ? data.queryStringObject.sort.trim() : 'DESC';

   if( 
    tokenHeader && 
    uuidHeader 
    ){

        token.verify(uuidHeader,tokenHeader).then((result)=>{
			
            if(!result){
                callback(400,{'Error':'Token Mismatch or expired'});
            }	

        })
        .then(()=>{
               
            if(param){
                uuidHeader = param;
            }           
            models.Friend 
                .findAll({where:{                        
                    [Op.or]: [
                        {userA:uuidHeader},
                        {userB:uuidHeader}
                    ]                            
                } })
                .then((friends) => {
                    // callback(200,friends)
                    let userIds = [];
                    //fetch all the user objects
                    friends.forEach((friend)=>{

                        let {userA,userB} = friend;
                        userA == uuidHeader ? userIds.push(userB) : userIds.push(userA);

                    });

                    return userIds;
                
                })
                .then((userIds)=>{
                    models.User.findAndCountAll({where: {id:userIds},include:[{model:models.Profile}], offset: page, limit: limit, order: [['createdAt', sort]]})
                    .then(user=>callback(200,{user}))
                    .catch(err=>callback(500,{"Error":err}));
                })
                .catch((err)=>{
                   
                    console.log(err);
                    callback(400,{'Error':err});
                });
            

        })
        .catch((err)=>{

            callback(500,{'Error':err});
            // callback(500,{'Error':"something went wrong, post not created"});
        });  

     

    }else{

		let errorObject = [];

		if(!tokenHeader){
			errorObject.push('Token you supplied is not valid or has expired');
		}
		if(!uuidHeader){
			errorObject.push('UUID you supplied is invalid or expired');
        }
        
     

		callback(400,{'Error':errorObject});

	}
    
    
}

/**
 * @api {delete} /friends/:uuid Delete Friend 
 * @apiName deleteFriend
 * @apiGroup Friends
 * @apiHeader {String} uuid Authorization UUID.
 * @apiHeader {String} Token Authorization Token.
 * @apiDescription The endpoint deletes a friend from a user list of friends
 * @apiParam {String} uuid uuid of the friend to be deleted 
 *@apiSuccessExample Success-Response:
 *HTTP/1.1 200 OK
 *{
 *   "Success": "Friend Deleted"
 *}
 *@apiErrorExample Error-Response:
 *HTTP/1.1 400 Bad Request
 *{
 *   "Error": [
 *       "Friend uuid not valid"
 *   ]
 *}
 * @apiErrorExample Error-Response:
 *HTTP/1.1 404 Bad Request
 *{
 *   "Error": [
 *       "Relationship not found"
 *   ]
 *}
 */

friends.delete = (data,callback)=>{

    let param = typeof(data.param) == 'string' && data.param.trim().length > 0 ? data.param.trim() : false;
	let tokenHeader = typeof(data.headers.token) == 'string' && data.headers.token.trim().length > 0 ? data.headers.token.trim() : false;
	let uuidHeader = typeof(data.headers.uuid) == 'string' && data.headers.uuid.trim() ? data.headers.uuid.trim() : false;

	if( 
		tokenHeader && 
		uuidHeader &&
		param 
		){

			token.verify(uuidHeader,tokenHeader).then((result)=>{
					
				if(!result){
						callback(400,{'Error':'Token Mismatch or expired'});
				}
	
			})
			.then(()=>{
				models.Friend.findOne({where: {id:param}})
				.then((friend)=>{
					if(friend){
						let destroy = friend.destroy();
						if(destroy){
							callback(404,{'Success':'Friend Deleted'});
						}else{
							callback(500,{'Error':'Friend not deleted, something went wrong'});
						}
					}else{
						callback(404,{'Error':'Friend not found'});	
					}
				})
				.catch(err=>callback(500,err));
			})
			.catch(err=>callback(500,err));


	}else{

		let errorObject = [];

		if(!token){
			errorObject.push('Token you supplied is not valid or expired');
		}
		if(!uuidHeader){
			errorObject.push('uuid in the header not found');
		}
		if(!param){
			errorObject.push('friend uuid not valid');
		}

		callback(400,{'Error':errorObject});
	}

}

module.exports = friends;