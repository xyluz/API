'use strict';
module.exports = (sequelize, DataTypes) => {
  const Reaction = sequelize.define('Reaction', {
    id:  {
      type: DataTypes.UUID,
      primaryKey: true, 
      defaultValue: DataTypes.UUIDV4
    },
    postId: DataTypes.UUID,
    userId: DataTypes.UUID
  }, {});
  Reaction.associate = function(models) {
    // associations can be defined here
    Reaction.belongsTo(models.User);
    Reaction.belongsTo(models.Post);
  };
  return Reaction;
};