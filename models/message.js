'use strict';
module.exports = (sequelize, DataTypes) => {
  const Message = sequelize.define('Message', {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4
    },
    sender: DataTypes.UUID,
    receiver: DataTypes.UUID,
    replyTo: DataTypes.UUID,
    message: DataTypes.TEXT

  }, {});
  Message.associate = function(models) {
    // associations can be defined here
  }; 
  return Message;
};