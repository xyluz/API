'use strict';
module.exports = (sequelize, DataTypes) => {
  const Friend = sequelize.define('Friend', {
     id:  {
      type: DataTypes.UUID,
      primaryKey: true, 
      defaultValue: DataTypes.UUIDV4
    },
    userA: DataTypes.UUID,
    userB: DataTypes.UUID,
    status: DataTypes.INTEGER

  }, {});
  Friend.associate = function(models) {
    // associations can be defined here
  };
  return Friend;
};