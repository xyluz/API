'use strict';
module.exports = (sequelize, DataTypes) => {
  const FriendRequest = sequelize.define('FriendRequest', {
    id:  {
      type: DataTypes.UUID,
      primaryKey: true, 
      defaultValue: DataTypes.UUIDV4
    },
    requestSender: DataTypes.UUID,
    requestReceiver: DataTypes.UUID,
    status: DataTypes.INTEGER
    
  }, {});
  FriendRequest.associate = function(models) {
    // associations can be defined here 
    // FriendRequest.belongsTo(models.User);

  };
  return FriendRequest;
};