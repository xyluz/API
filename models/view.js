'use strict';
module.exports = (sequelize, DataTypes) => {
  const View = sequelize.define('View', {
    id:  {
      type: DataTypes.UUID,
      primaryKey: true, 
      defaultValue: DataTypes.UUIDV4
    }, 
    postId: DataTypes.UUID,
    userId: DataTypes.UUID,
    status: DataTypes.INTEGER
  }, {});
  View.associate = function(models) {
    // associations can be defined here
    View.belongsTo(models.Post);
    View.belongsTo(models.User);
  };
  return View;
};