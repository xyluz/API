'use strict';
module.exports = (sequelize, DataTypes) => {
  const Share = sequelize.define('Share', {
    id:  {
      type: DataTypes.UUID,
      primaryKey: true, 
      defaultValue: DataTypes.UUIDV4
    },
    postId: DataTypes.UUID,
    userId: DataTypes.UUID,
    status: DataTypes.INTEGER
  }, {});
  Share.associate = function(models) {
    // associations can be defined here
    Share.belongsTo(models.Post);
    Share.belongsTo(models.User);
  };
  return Share;
};