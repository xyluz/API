'use strict';
module.exports = (sequelize, DataTypes) => { 
  const Post = sequelize.define('Post', {
    id: {
      type: DataTypes.UUID,
      primaryKey: true, 
      defaultValue: DataTypes.UUIDV4
    },
    userId: DataTypes.UUID,
    status: DataTypes.INTEGER,
    location: DataTypes.STRING, 
    attachment: DataTypes.TEXT,
    post: DataTypes.TEXT, 
    post_type: DataTypes.STRING
  }, {});
  Post.associate = function(models) {
    // associations can be defined here
    Post.belongsTo(models.User);

    Post.hasMany(models.View, {
      foreignKey: 'postId'
    });

    Post.hasMany(models.Share, {
      foreignKey: 'postId'
    });

    Post.hasMany(models.Comment, {
      foreignKey: 'postId'
    });

    Post.hasMany(models.Reaction, {
      foreignKey: 'postId'
    });
    
  };
  return Post;
};