'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Shares', {
      id: {
        allowNull: false,
        type: Sequelize.UUID,
        primaryKey: true 
      },
      postId: {
        type: Sequelize.UUID,
        allowNull:false
      },
      userId: {
        type: Sequelize.UUID,
        allowNull:false
      },
      status: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Shares');
  }
};