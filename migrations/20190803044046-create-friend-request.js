'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('FriendRequests', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID
      },
      requestSender: { //request sender
        type: Sequelize.UUID,
        allowNull:false
      },
      requestReceiver: {
        type: Sequelize.UUID,
        allowNull: false 
      },
      status: {
        type: Sequelize.INTEGER,
        defaultValue: 1
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('FriendRequests');
  }
};