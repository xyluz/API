'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Posts', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      uuid: {
        type: Sequelize.UUID,
        allowNull:false,
        defaultValue: Sequelize.UUIDV4
      },
      userId: {
        type: Sequelize.UUID,
        allowNull: false
      },
      status: {
        type: Sequelize.INTEGER,
        defaultValua: 1
      },
      location: {
        type: Sequelize.STRING,
        allowNull: true
      },
      attachment: {
        type: Sequelize.TEXT,
        allowNull: true
      },
      post: {
        type: Sequelize.TEXT,
        allowNull: false
      },
      post_type: {
        type: Sequelize.STRING,
        defaultValua: 'post'
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Posts');
  }
};